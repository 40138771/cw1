﻿/* 
*
*Written by Okorie Chinaza Medeline 9/10/2014
*The class Program is the main entry point for the application or gui.
* It runs the gui window as an application.
* 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cw1
{
    class Program
    {
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new GUI());

            
        }
    }
}
