﻿/* 
*
*Written by Okorie Chinaza Medeline 16/10/2014
* The class PaySlip contains code that enable and disable the second window called the Pay Slip when the calculate Pay button on the gui window is clicked
* 
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cw1
{
    public partial class PaySlip : Form 
    {

        public PaySlip(Staff staff)
        {
            String firstName; string secondName; double hoursWorked; double pay; //double taxRate;

       
            InitializeComponent();

            secondName = staff.SecondName;
            lblPaySurname.Text = secondName;

            firstName =  staff.FirstName ;
            lblPayFirstname.Text = firstName ;

            pay = staff.CalcPay((int)staff.HoursWorked);
            lblPayGP.Text = pay.ToString() ;
            

            hoursWorked =  staff.HoursWorked ;
            lblPayHW.Text = hoursWorked.ToString();

            
            lblPayNP.Text = pay.ToString();

           

        }

        // This enables the close button on the payslip form to close application when clicked
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SecondWindow_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}


