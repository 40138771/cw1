﻿/* 
*
*Written by Okorie Chinaza Medeline 23/10/2014
* The class PhdStudent inherits from the Staff class
* 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cw1
{
    class PhdStudent : Staff
    {
        private string course;
        private string supervisor;

        //This method contains the get and set methods and validation checks for property course
        public string Course
        {
            get
            {
                return course;
            }
            set
            {
                course = value;
            }
        }

        //This method contains the get and set methods and validation checks for property supervisor
        public string Supervisor
        {
            get
            {
                return supervisor;
            }
            set
            {
                supervisor = value;
            }
        }
    }
}
