﻿namespace Cw1
{
    partial class PaySlip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPayslip = new System.Windows.Forms.Label();
            this.lblHoursWorked = new System.Windows.Forms.Label();
            this.lblGrossPay = new System.Windows.Forms.Label();
            this.lblTaxRate = new System.Windows.Forms.Label();
            this.lblNetPay = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblPaySurname = new System.Windows.Forms.Label();
            this.lblPayFirstname = new System.Windows.Forms.Label();
            this.lblPayHW = new System.Windows.Forms.Label();
            this.lblPayGP = new System.Windows.Forms.Label();
            this.lblPayTax = new System.Windows.Forms.Label();
            this.lblPayNP = new System.Windows.Forms.Label();
            this.lblPayDepartment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPayslip
            // 
            this.lblPayslip.AutoSize = true;
            this.lblPayslip.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.35F);
            this.lblPayslip.Location = new System.Drawing.Point(20, 24);
            this.lblPayslip.Name = "lblPayslip";
            this.lblPayslip.Size = new System.Drawing.Size(53, 17);
            this.lblPayslip.TabIndex = 0;
            this.lblPayslip.Text = "Payslip";
            // 
            // lblHoursWorked
            // 
            this.lblHoursWorked.AutoSize = true;
            this.lblHoursWorked.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.35F);
            this.lblHoursWorked.Location = new System.Drawing.Point(20, 70);
            this.lblHoursWorked.Name = "lblHoursWorked";
            this.lblHoursWorked.Size = new System.Drawing.Size(99, 17);
            this.lblHoursWorked.TabIndex = 1;
            this.lblHoursWorked.Text = "Hours Worked";
            // 
            // lblGrossPay
            // 
            this.lblGrossPay.AutoSize = true;
            this.lblGrossPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.35F);
            this.lblGrossPay.Location = new System.Drawing.Point(20, 123);
            this.lblGrossPay.Name = "lblGrossPay";
            this.lblGrossPay.Size = new System.Drawing.Size(74, 17);
            this.lblGrossPay.TabIndex = 2;
            this.lblGrossPay.Text = "Gross Pay";
            // 
            // lblTaxRate
            // 
            this.lblTaxRate.AutoSize = true;
            this.lblTaxRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.35F);
            this.lblTaxRate.Location = new System.Drawing.Point(20, 175);
            this.lblTaxRate.Name = "lblTaxRate";
            this.lblTaxRate.Size = new System.Drawing.Size(65, 17);
            this.lblTaxRate.TabIndex = 3;
            this.lblTaxRate.Text = "Tax Rate";
            // 
            // lblNetPay
            // 
            this.lblNetPay.AutoSize = true;
            this.lblNetPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.35F);
            this.lblNetPay.Location = new System.Drawing.Point(20, 229);
            this.lblNetPay.Name = "lblNetPay";
            this.lblNetPay.Size = new System.Drawing.Size(58, 17);
            this.lblNetPay.TabIndex = 4;
            this.lblNetPay.Text = "Net Pay";
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.35F);
            this.btnClose.Location = new System.Drawing.Point(257, 260);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(74, 31);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblPaySurname
            // 
            this.lblPaySurname.AutoSize = true;
            this.lblPaySurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.lblPaySurname.Location = new System.Drawing.Point(110, 24);
            this.lblPaySurname.Name = "lblPaySurname";
            this.lblPaySurname.Size = new System.Drawing.Size(76, 16);
            this.lblPaySurname.TabIndex = 6;
            this.lblPaySurname.Text = "<Surname>";
            // 
            // lblPayFirstname
            // 
            this.lblPayFirstname.AutoSize = true;
            this.lblPayFirstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.lblPayFirstname.Location = new System.Drawing.Point(204, 24);
            this.lblPayFirstname.Name = "lblPayFirstname";
            this.lblPayFirstname.Size = new System.Drawing.Size(81, 16);
            this.lblPayFirstname.TabIndex = 7;
            this.lblPayFirstname.Text = "<Firstname>";
            // 
            // lblPayHW
            // 
            this.lblPayHW.AutoSize = true;
            this.lblPayHW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.lblPayHW.Location = new System.Drawing.Point(171, 74);
            this.lblPayHW.Name = "lblPayHW";
            this.lblPayHW.Size = new System.Drawing.Size(103, 16);
            this.lblPayHW.TabIndex = 8;
            this.lblPayHW.Text = "<hoursWorked>";
            // 
            // lblPayGP
            // 
            this.lblPayGP.AutoSize = true;
            this.lblPayGP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.lblPayGP.Location = new System.Drawing.Point(171, 124);
            this.lblPayGP.Name = "lblPayGP";
            this.lblPayGP.Size = new System.Drawing.Size(80, 16);
            this.lblPayGP.TabIndex = 9;
            this.lblPayGP.Text = "<grossPay>";
            // 
            // lblPayTax
            // 
            this.lblPayTax.AutoSize = true;
            this.lblPayTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.lblPayTax.Location = new System.Drawing.Point(171, 179);
            this.lblPayTax.Name = "lblPayTax";
            this.lblPayTax.Size = new System.Drawing.Size(39, 16);
            this.lblPayTax.TabIndex = 10;
            this.lblPayTax.Text = "<tax>";
            // 
            // lblPayNP
            // 
            this.lblPayNP.AutoSize = true;
            this.lblPayNP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.lblPayNP.Location = new System.Drawing.Point(171, 233);
            this.lblPayNP.Name = "lblPayNP";
            this.lblPayNP.Size = new System.Drawing.Size(67, 16);
            this.lblPayNP.TabIndex = 11;
            this.lblPayNP.Text = "<NetPay>";
            // 
            // lblPayDepartment
            // 
            this.lblPayDepartment.AutoSize = true;
            this.lblPayDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.lblPayDepartment.Location = new System.Drawing.Point(291, 24);
            this.lblPayDepartment.Name = "lblPayDepartment";
            this.lblPayDepartment.Size = new System.Drawing.Size(98, 16);
            this.lblPayDepartment.TabIndex = 12;
            this.lblPayDepartment.Text = "(<department>)";
            // 
            // PaySlip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 295);
            this.Controls.Add(this.lblPayDepartment);
            this.Controls.Add(this.lblPayNP);
            this.Controls.Add(this.lblPayTax);
            this.Controls.Add(this.lblPayGP);
            this.Controls.Add(this.lblPayHW);
            this.Controls.Add(this.lblPayFirstname);
            this.Controls.Add(this.lblPaySurname);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblNetPay);
            this.Controls.Add(this.lblTaxRate);
            this.Controls.Add(this.lblGrossPay);
            this.Controls.Add(this.lblHoursWorked);
            this.Controls.Add(this.lblPayslip);
            this.Name = "PaySlip";
            this.Text = "PaySlip";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPayslip;
        private System.Windows.Forms.Label lblHoursWorked;
        private System.Windows.Forms.Label lblGrossPay;
        private System.Windows.Forms.Label lblTaxRate;
        private System.Windows.Forms.Label lblNetPay;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblPaySurname;
        private System.Windows.Forms.Label lblPayFirstname;
        private System.Windows.Forms.Label lblPayHW;
        private System.Windows.Forms.Label lblPayGP;
        private System.Windows.Forms.Label lblPayTax;
        private System.Windows.Forms.Label lblPayNP;
        private System.Windows.Forms.Label lblPayDepartment;
    }
}