﻿/* 
*
* Written by Okorie Chinaza Medeline 9/10/2014
* The class Gui contains code that enable the functions of the tools to work as specified in the first window called Gui
* 
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cw1
{
    public partial class GUI : Form
    {
        Staff staff = new Staff(); //new Staff class object staff 
        
        
        public GUI()
        {
            InitializeComponent();
        }

        private void MyForm_Load(object sender, EventArgs e)
        { }

        // This enables the set button on the gui form to update Staff class with values in textboxes when clicked
        private void btnSet_Click(object sender, EventArgs e)
        {
            if (tBoxFirstName.Text == "" | tBoxSecondName.Text == "" | tBoxDateOfBirth.Text == "" | tBoxDepartment.Text == ""
              | (tBoxID.Text == "" | tBoxPayRate.Text == "" | tBoxHoursWorked.Text == ""))// Validation to check that no textbox is left blank
            {
                MessageBox.Show("Enter all fields !");  // Error message if the statement is false
            }

            else
            {
                //saves the text in the textbox

                string firstName = tBoxFirstName.Text;
                staff.FirstName = firstName;


                string secondName = tBoxSecondName.Text;
                staff.SecondName = secondName;


                string dateOfBirth = tBoxDateOfBirth.Text;
                staff.DateOfBirth = dateOfBirth;


                string department = tBoxDepartment.Text;
                staff.Department = department;


                int id = int.Parse(tBoxID.Text); // converts string input to int
                staff.Id = id;


                int payRate = int.Parse(tBoxPayRate.Text);
                staff.PayRate = payRate;


                double hoursWorked = double.Parse(tBoxHoursWorked.Text); // converts string input to double
                staff.HoursWorked = hoursWorked;
  
            }
        }

        // This enables the clear button on the gui form to remove contents of the textboxes when clicked
        private void btnClear_Click(object sender, EventArgs e)
        {
            // clears the text in the textboxes
            tBoxID.Clear();

            tBoxFirstName.Clear();

            tBoxSecondName.Clear();

            tBoxDateOfBirth.Clear();

            tBoxDepartment.Clear();

            tBoxPayRate.Clear();

            tBoxHoursWorked.Clear();

        }

        // This enables the get button on the gui form to use values saved in Staff class to update textboxes when clicked
        private void btnGet_Click(object sender, EventArgs e)
        {
     
            tBoxFirstName.Text = staff.FirstName;

            tBoxSecondName.Text = staff.SecondName;

            tBoxDateOfBirth.Text = staff.DateOfBirth;

            tBoxDepartment.Text = staff.Department;

            tBoxPayRate.Text = Convert.ToString(staff.PayRate);

            tBoxID.Text = Convert.ToString(staff.Id);

            tBoxHoursWorked.Text = Convert.ToString(staff.HoursWorked);
        }


        // This enables the calculate pay button on the gui form to show payslip window when clicked
        private void btnCalcPay_Click(object sender, EventArgs e)
      {
          PaySlip payWindow = new PaySlip(staff);
          payWindow.ShowDialog();
         
             }

        }
   }
