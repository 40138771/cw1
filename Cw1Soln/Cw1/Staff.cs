﻿/* 
*
*Written by Okorie Chinaza Medeline 9/10/2014
* The class Staff contains code that initialize and enable properties that concerns the staff in the firm
* 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cw1
{
    public class Staff
    {
        // Declared all variables or properties
        private int id;
        private string firstName;
        private string secondName;
        private string dateOfBirth;
        private string department;
        private double payRate;
        private double hoursWorked;
        private double pay;


        //This method contains the get and set methods and validation checks for property staffId
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                if (value < 1000 || value > 2000) // The if statement is a validation that value is in range 1000 to 2000
                {
                    throw new ArgumentException("Error : Number out of range"); //Error if the statement is false
                }
                else
                {
                    id = value;
                }
            }
        }

        //This method contains the get and set methods and validation checks for property first name
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (value.Equals("")) // The if statement is a validation that textbox is not empty
                {
                    firstName = "wrong input. Please retype"; //Error if the statement is false
                }
                else
                {
                    firstName = value;
                }
            }
        }

        //This method contains the get and set methods and validation checks for property second Name
        public string SecondName
        {
            get
            {
                return secondName;
            }
            set
            {
                if (value.Equals("")) // The if statement is a validation that textbox is not empty
                {
                    secondName = "wrong input. Please retype"; //Error if the statement is false
                }
                else
                {
                    secondName = value;
                }
            }
        }

        //This method contains the get and set methods and validation checks for property date of birth
        public string DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                if (value.Equals("")) // The if statement is a validation that textbox is not empty
                {
                    dateOfBirth = "wrong input. Please retype"; //Error if the statement is false
                }
                else
                {
                    dateOfBirth = value;
                }
            }
        }

        //This method contains the get and set methods and validation checks for property department
        public string Department
        {
            get
            {
                return department;
            }
            set
            {
                if (value.Equals("")) // The if statement is a validation that textbox is not empty
                {
                    department = "wrong input. Please retype"; //Error if the statement is false
                }
                else
                {
                    department = value;
                }
            }
        }

        //This method contains the get and set methods and validation checks for property pay rate
        public double PayRate
        {
            get
            {
                return payRate;
            }
            set
            {
                if (value < 0 || value > 99999) // The if statement is a validation that value is in range 0 to 99999
                {
                    throw new ArgumentException("Error : Number out of range"); //Error if the statement is false
                }
                else
                {
                    payRate = value;
                }
            }
        }

        //This method contains the get and set methods and validation checks for property hours worked
        public double HoursWorked
        {
            get
            {
                return hoursWorked;
            }
            set
            {
                if (value < 1 || value > 65) // The if statement is a validation that value is in range 1 to 65
                {
                    throw new ArgumentException("Error : Number out of range"); //Error if the statement is false
                }
                else
                {
                    hoursWorked = value;
                }
            }
        }

        // This method returns the tax due to the value of gross pay
        public double  CalcTaxRate(int grossPay) //in pence
        {
            double taxRate = 0;

            if (grossPay <= 100000)

                taxRate = 10;

            else taxRate = 20;

            return taxRate;
        }

        // This method calculates the payment 
        public double CalcPay(int hours)
        {
            pay = ((hours * payRate));
            return pay;
        }

       

    }
}
