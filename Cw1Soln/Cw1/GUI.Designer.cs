﻿namespace Cw1
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblDateOfBirth = new System.Windows.Forms.Label();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.lblStaffId = new System.Windows.Forms.Label();
            this.lblPayRate = new System.Windows.Forms.Label();
            this.lblText = new System.Windows.Forms.Label();
            this.lblHoursWorkd = new System.Windows.Forms.Label();
            this.tBoxPayRate = new System.Windows.Forms.TextBox();
            this.tBoxID = new System.Windows.Forms.TextBox();
            this.tBoxDepartment = new System.Windows.Forms.TextBox();
            this.tBoxDateOfBirth = new System.Windows.Forms.TextBox();
            this.tBoxSecondName = new System.Windows.Forms.TextBox();
            this.tBoxFirstName = new System.Windows.Forms.TextBox();
            this.tBoxHoursWorked = new System.Windows.Forms.TextBox();
            this.btnSet = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnGet = new System.Windows.Forms.Button();
            this.btnCalcPay = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.32F);
            this.lblName.Location = new System.Drawing.Point(16, 29);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(53, 20);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Name";
            // 
            // lblDateOfBirth
            // 
            this.lblDateOfBirth.AutoSize = true;
            this.lblDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.35F);
            this.lblDateOfBirth.Location = new System.Drawing.Point(16, 88);
            this.lblDateOfBirth.Name = "lblDateOfBirth";
            this.lblDateOfBirth.Size = new System.Drawing.Size(105, 20);
            this.lblDateOfBirth.TabIndex = 1;
            this.lblDateOfBirth.Text = "Date of Birth";
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.35F);
            this.lblDepartment.Location = new System.Drawing.Point(16, 148);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(97, 20);
            this.lblDepartment.TabIndex = 2;
            this.lblDepartment.Text = "Department";
            // 
            // lblStaffId
            // 
            this.lblStaffId.AutoSize = true;
            this.lblStaffId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.35F);
            this.lblStaffId.Location = new System.Drawing.Point(16, 199);
            this.lblStaffId.Name = "lblStaffId";
            this.lblStaffId.Size = new System.Drawing.Size(66, 20);
            this.lblStaffId.TabIndex = 3;
            this.lblStaffId.Text = "Staff ID";
            // 
            // lblPayRate
            // 
            this.lblPayRate.AutoSize = true;
            this.lblPayRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.35F);
            this.lblPayRate.Location = new System.Drawing.Point(16, 250);
            this.lblPayRate.Name = "lblPayRate";
            this.lblPayRate.Size = new System.Drawing.Size(77, 20);
            this.lblPayRate.TabIndex = 4;
            this.lblPayRate.Text = "Pay Rate";
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.1F);
            this.lblText.Location = new System.Drawing.Point(12, 270);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(159, 18);
            this.lblText.TabIndex = 5;
            this.lblText.Text = "(enter in pounds 000.0)";
            // 
            // lblHoursWorkd
            // 
            this.lblHoursWorkd.AutoSize = true;
            this.lblHoursWorkd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.35F);
            this.lblHoursWorkd.Location = new System.Drawing.Point(16, 341);
            this.lblHoursWorkd.Name = "lblHoursWorkd";
            this.lblHoursWorkd.Size = new System.Drawing.Size(117, 20);
            this.lblHoursWorkd.TabIndex = 7;
            this.lblHoursWorkd.Text = "Hours Worked";
            // 
            // tBoxPayRate
            // 
            this.tBoxPayRate.Location = new System.Drawing.Point(182, 268);
            this.tBoxPayRate.Name = "tBoxPayRate";
            this.tBoxPayRate.Size = new System.Drawing.Size(100, 20);
            this.tBoxPayRate.TabIndex = 8;
            // 
            // tBoxID
            // 
            this.tBoxID.Location = new System.Drawing.Point(182, 201);
            this.tBoxID.Name = "tBoxID";
            this.tBoxID.Size = new System.Drawing.Size(100, 20);
            this.tBoxID.TabIndex = 9;
            // 
            // tBoxDepartment
            // 
            this.tBoxDepartment.Location = new System.Drawing.Point(182, 150);
            this.tBoxDepartment.Name = "tBoxDepartment";
            this.tBoxDepartment.Size = new System.Drawing.Size(100, 20);
            this.tBoxDepartment.TabIndex = 10;
            // 
            // tBoxDateOfBirth
            // 
            this.tBoxDateOfBirth.Location = new System.Drawing.Point(182, 90);
            this.tBoxDateOfBirth.Name = "tBoxDateOfBirth";
            this.tBoxDateOfBirth.Size = new System.Drawing.Size(100, 20);
            this.tBoxDateOfBirth.TabIndex = 11;
            // 
            // tBoxSecondName
            // 
            this.tBoxSecondName.Location = new System.Drawing.Point(319, 29);
            this.tBoxSecondName.Name = "tBoxSecondName";
            this.tBoxSecondName.Size = new System.Drawing.Size(100, 20);
            this.tBoxSecondName.TabIndex = 12;
            // 
            // tBoxFirstName
            // 
            this.tBoxFirstName.Location = new System.Drawing.Point(182, 29);
            this.tBoxFirstName.Name = "tBoxFirstName";
            this.tBoxFirstName.Size = new System.Drawing.Size(100, 20);
            this.tBoxFirstName.TabIndex = 13;
            // 
            // tBoxHoursWorked
            // 
            this.tBoxHoursWorked.Location = new System.Drawing.Point(182, 343);
            this.tBoxHoursWorked.Name = "tBoxHoursWorked";
            this.tBoxHoursWorked.Size = new System.Drawing.Size(100, 20);
            this.tBoxHoursWorked.TabIndex = 14;
            // 
            // btnSet
            // 
            this.btnSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.35F);
            this.btnSet.Location = new System.Drawing.Point(15, 402);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(54, 25);
            this.btnSet.TabIndex = 15;
            this.btnSet.Text = "Set";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.35F);
            this.btnClear.Location = new System.Drawing.Point(105, 402);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(66, 25);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnGet
            // 
            this.btnGet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.35F);
            this.btnGet.Location = new System.Drawing.Point(207, 402);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(60, 25);
            this.btnGet.TabIndex = 17;
            this.btnGet.Text = "Get";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // btnCalcPay
            // 
            this.btnCalcPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.35F);
            this.btnCalcPay.Location = new System.Drawing.Point(296, 402);
            this.btnCalcPay.Name = "btnCalcPay";
            this.btnCalcPay.Size = new System.Drawing.Size(123, 25);
            this.btnCalcPay.TabIndex = 18;
            this.btnCalcPay.Text = "Calculate Pay";
            this.btnCalcPay.UseVisualStyleBackColor = true;
            
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 440);
            this.Controls.Add(this.btnCalcPay);
            this.Controls.Add(this.btnGet);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.tBoxHoursWorked);
            this.Controls.Add(this.tBoxFirstName);
            this.Controls.Add(this.tBoxSecondName);
            this.Controls.Add(this.tBoxDateOfBirth);
            this.Controls.Add(this.tBoxDepartment);
            this.Controls.Add(this.tBoxID);
            this.Controls.Add(this.tBoxPayRate);
            this.Controls.Add(this.lblHoursWorkd);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.lblPayRate);
            this.Controls.Add(this.lblStaffId);
            this.Controls.Add(this.lblDepartment);
            this.Controls.Add(this.lblDateOfBirth);
            this.Controls.Add(this.lblName);
            this.Name = "GUI";
            this.Text = "GUI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDateOfBirth;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.Label lblStaffId;
        private System.Windows.Forms.Label lblPayRate;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Label lblHoursWorkd;
        private System.Windows.Forms.TextBox tBoxPayRate;
        private System.Windows.Forms.TextBox tBoxID;
        private System.Windows.Forms.TextBox tBoxDepartment;
        private System.Windows.Forms.TextBox tBoxDateOfBirth;
        private System.Windows.Forms.TextBox tBoxSecondName;
        private System.Windows.Forms.TextBox tBoxFirstName;
        private System.Windows.Forms.TextBox tBoxHoursWorked;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.Button btnCalcPay;
    }
}